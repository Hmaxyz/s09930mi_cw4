import sys
import argparse

def version():
    print('Version: WC V0.4')
    exit(0)

def longline(lines):
    lengths = []
    for line in lines:
        length = len(line)
        lengths.append(length)
    return max(lengths)

def formatter(lines=0, words=0, bytest=0, chars=0, longest=0, filename=0):
    myformat=""
    plists = [lines, words, bytest, chars, longest, filename]
    
    for p in plists:
        if p is not 0:
            myformat+=str(p) + "\t"
    
    print(myformat)

def charcount(lines):
    char=0
    for line in lines:
        length = len(line)
        char+=length

    return char
    

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("a", nargs="*")
    parser.add_argument('--version', help='Print version of WC.', required=False, action='store_true')
    parser.add_argument('-l','--lines', help='Print the newline counts.', required=False, action='store_true')
    parser.add_argument('-w','--words', help='Print the word counts.', required=False, action='store_true')
    parser.add_argument('-c','--bytes', help='Print the byte counts.', required=False, action='store_true')
    parser.add_argument('-m','--chars', help='Print the character counts.', required=False, action='store_true')
    parser.add_argument('-L','--longest', help='Print the longest line.', required=False, action='store_true')
    parser.add_argument('--files0-from', help='read input from the files specified by NUL-terminated names in file F; If F is - then read names from standard input', required=False)

    args = parser.parse_args()

    if args.version:
        version()

    if args.files0_from:
        input=sys.stdin


    files = args.a
    for file in files:
        filename = file.strip()

        try:     
            file = open(filename, 'r', encoding="utf-8")
            content = file.read()
            
            linestmp = content.split('\n')

            
            lines=len(content.split('\n'))
            words=len(content.split(' ')) 
            bytest=0
            chars=charcount(linestmp)
            longest=longline(linestmp)
            filesize=len(content.encode('utf-8'))


            if not args.lines and not args.chars and not args.bytes and not args.words and not args.longest:
                print("{}\t{}\t{}\t{}".format(lines, words, filesize, filename))
                exit(0)

            if not args.lines:
                lines = 0
            if not args.longest:
                longest = 0
            if not args.chars:
                chars = 0
            if not args.bytes:
                filesize = 0
            if not args.words:
                words = 0

            file.close()
            
            formatter(lines, words, filesize, chars, longest, filename)

        except FileNotFoundError as e:
            print(e)
            continue
        
        
        
        



if __name__== '__main__':
    main()

    
    
